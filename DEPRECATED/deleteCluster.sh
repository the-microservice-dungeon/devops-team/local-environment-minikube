#!/bin/bash

# CONFIG VARIABLES
LIGHT_GREEN='\033[1;32m'

Text_Light_Green(){
  echo -e "${LIGHT_GREEN} $1 ${NOCOLOR}"
}


minikube stop
minikube delete --all

Text_Light_Green " minikube is stopped and all profiles are deleted"