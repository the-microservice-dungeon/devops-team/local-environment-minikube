#!/bin/bash

# BASH CONFIG VARIABLES
BLUE='\033[1;34m'
RED='\033[0;31m'
NOCOLOR='\033[0m'
GREEN='\033[0;32m'
DARK_GREY='\033[1;30m'
MIN_MEMORY_REQUIRED=8120
MIN_CORES_REQUIRED=6
AVAILABLE_MEMORY=0
AVAILABLE_CORES=0



# VARIABLES
# USE_KUBERNETES_VERSION=1.24.0
USE_KUBERNETES_VERSION=1.28.3

Text_Red () {
  echo -e "${RED} $1 ${NOCOLOR}"
}

Text_Green(){
  echo -e "${GREEN} $1 ${NOCOLOR}"
}

Text_Blue(){
  echo -e "${BLUE} $1 ${NOCOLOR}"
}

Text_Dark_Grey(){
  echo -e "${DARK_GREY} $1 ${NOCOLOR}"
}

Help(){
  echo " "
  echo "help list only for first installation"
  echo " "
  echo "  =======> Generally Commands <======="
  echo " "

  echo "  -h | --help       ===> for this help"
  echo " "
  echo " "
  echo " "
  echo "  =======> Install Core Service <======="
  echo " "

  echo "  --allservice   ===> for install all core services"
  echo "   --noservice   ===> for install no core services"
  echo " "
  echo "  -ro | --robot        ===> for install robot core services"
  echo "  -ma | --map          ===> for install map core services"
  echo "  -tr | --trading      ===> for install trading core services"
  echo "  -ga | --game         ===> for install game core services"
  echo "  -gl | --gamelog      ===> for install gamelog core services"

  echo " "
  echo "  =======> install_for_insecure_registry <======="
  echo " "
  echo "  -ins   ===> install_for_insecure_registry"
  echo "    => After choosing this option enter your IP Address and Port"
  echo "    => Core-Services will not be installed"
  echo "    => you will install Core-Services than run following"
  echo "    => ./firstBuildCluster.sh -ins --allservice"

  echo " "
  echo "example: install only robot and game "
  echo " ./core-service-interaction.sh -ro -ga"
  echo " OR"
  echo " ./core-service-interaction.sh -ga -ro"
  echo " "
  echo "the PARAMETERS position does not matter "
  echo " "

}




# BASH PARAMETERS

install_service=true
no_install_service=false


install_robot=$no_install_service
install_trading=$no_install_service
install_map=$no_install_service
install_game=$no_install_service
install_gamelog=$no_install_service

install_for_insecure_registry=false

No_Core_Services(){
  install_robot=$no_install_service
  install_map=$no_install_service
  install_trading=$no_install_service
  install_game=$no_install_service
  install_gamelog=$no_install_service
}

All_Core_Services(){
  install_robot=$install_service
  install_map=$install_service
  install_trading=$install_service
  install_game=$install_service
  install_gamelog=$install_service
}

Check_Resources() {
  if [[ "$OSTYPE" == "linux-gnu"* ]]; then
    AVAILABLE_MEMORY=$(grep MemTotal /proc/meminfo | awk '{print $2}')
    AVAILABLE_CORES=$(grep -c ^processor /proc/cpuinfo)
    AVAILABLE_MEMORY=$((AVAILABLE_MEMORY / 1024)) # convert from KB to MB
  elif [[ "$OSTYPE" == "darwin"* ]]; then
    AVAILABLE_MEMORY=$(sysctl -n hw.memsize)
    AVAILABLE_CORES=$(sysctl -n hw.ncpu)
    AVAILABLE_MEMORY=$((AVAILABLE_MEMORY / 1024 / 1024)) # convert from bytes to MB
  else
    Text_Red "Unsupported OS. This script supports Linux and macOS only."
    exit 1
  fi

  if [[ AVAILABLE_MEMORY -ge MIN_MEMORY_REQUIRED ]] && [[ AVAILABLE_CORES -ge MIN_CORES_REQUIRED ]]; then
    Text_Green "Sufficient resources available: ${AVAILABLE_MEMORY} MB memory, ${AVAILABLE_CORES} cores."
    return 0
  else
    Text_Red "Insufficient resources: ${AVAILABLE_MEMORY} MB memory, ${AVAILABLE_CORES} cores."
    exit 1
  fi
}


if [ $# == 0 ] ; then
  All_Core_Services
  else
    for var in "$@" ; do
      case $var in

    -h | --help )
      Help
      exit
      ;;

    --noservice)
      No_Core_Services
      break
      ;;

    --allservice)
      All_Core_Services
      break
      ;;

    -ro | --robot)
      install_robot=$install_service
      ;;

    -ma | --map)
      install_map=$install_service
      ;;

    -tr | --trading)
      install_trading=$install_service
      ;;

    -ga | --game)
      install_game=$install_service
      ;;

    -gl | --gamelog)
      install_gamelog=$install_service
      ;;

    -ins)
      install_for_insecure_registry=true

      ;;

    *)
      echo "min. one parameter is not valid"
      exit
      ;;
    esac
  done
fi

# FUNCTIONS


Start_Show_Choosen_Services(){

  echo " "

  echo -e "${GREEN} you choose the following Core-Service  for this minikube installation"
  echo " "
  echo "install robot is $install_robot"
  echo "install trading is $install_trading"
  echo "install map is $install_map"
  echo "install game is $install_game"
  echo "install gamelog is $install_gamelog"
  echo " "
  echo " "
  echo -e " ${NOCOLOR}"

}


# CHECK NEEDED PROGRAMS
Check_Is_Installed(){
if ! command -v "$1" >/dev/null; then
  Text_Red "This script requires {$1} to be installed and on your PATH ..."
  exit 1
fi
}


# minikube
Check_Is_Installed 'minikube'
# helm
Check_Is_Installed 'helm'
# kubectl
Check_Is_Installed 'kubectl'
# git
Check_Is_Installed 'git'



# Start installation

Text_Green "Welcome, this script install minikube with everything needed"
Text_Green "     this should be finished in 3 minutes "

Start_Show_Choosen_Services

# Check resources
Check_Resources
# build minikube
Text_Blue "  -----> start build cluster "

if $install_for_insecure_registry ; then
  Text_Green "You choose installation for insecure private registry"
  Text_Green " make sure that you set up your Docker correctly"
  read -p "   Please enter your host IP Address and Port now: " ip_address port
  Text_Green " you entered IP Address $ip_address and Port $port is this correct?"
  read -p "   Please enter (y)es or (n)o with a blank between chars " current_ip current_port



  while [ $current_ip == n ] || [ "$current_port" == n ]; do
    if [ $current_ip == n ] && [ "$current_port" == n ]; then
      read -p "   Please enter your host IP Address and Port now (with a blank between both): " ip_address port
      Text_Green " you entered IP Address $ip_address and Port $port is this correct?"
      read -p "   Please enter (y)es or (n)o with a blank between chars" current_ip current_port
    fi

    if  [ $current_ip == n ] && [ "$current_port" == y ]; then
      read -p "   Please enter your host IP Address now: " ip_address
      Text_Green " you entered IP Address $ip_address is this correct?"
      read -p "   Please enter (y)es or (n)o " current_ip
    fi

    if  [ "$current_ip" == y ] && [ "$current_port" == n ]; then
      read -p "   Please enter your Port now: "  port
      Text_Green " you entered Port $port is this correct?"
      read -p "   Please enter (y)es or (n)o with a blank between chars "  current_port
    fi
  done

  Text_Green " you entered IP Address $ip_address and Port $port"
  Text_Green " =====> your registry will be at $ip_address:$port"

  # the setting --static-ip does not effect windows wsl2 linux
  # it is for access core services on minikube over fixed ip address
  minikube start --kubernetes-version="$USE_KUBERNETES_VERSION" --driver=docker --static-ip="10.10.10.10"  --insecure-registry="$ip_address:$port" --memory="$MIN_MEMORY_REQUIRED" --cpus="$MIN_CORES_REQUIRED"
  else
       minikube start --kubernetes-version="$USE_KUBERNETES_VERSION" --driver=docker --static-ip="10.10.10.10" --memory="$MIN_MEMORY_REQUIRED" --cpus="$MIN_CORES_REQUIRED"
fi


minikube status
# echo " --- minikube created"

Text_Blue "  ------> start install infrastructure on minikube "
Text_Blue "  ------> this take up to 2 minutes"



# INSTALL INFRASTRUCTURE
./installInfrastructure.sh




# INSTALL CORE SERVICES
Text_Blue "  -----> start install core services on minikube "

#./core-service-interaction.sh --update-repo

# Initialize an empty string for the parameters
params=""

# Append the respective parameters based on the conditions
if $install_robot; then
  params+="-ro "
fi

if $install_map; then
  params+="-ma "
fi

if $install_trading; then
  params+="-tr "
fi

if $install_game; then
  params+="-ga "
fi

if $install_gamelog; then
  params+="-gl "
fi

# Trim trailing space
params=$(echo "$params" | sed 's/ *$//')

if [ -n "$params" ]; then
  ./core-service-interaction.sh $params
fi

Text_Blue "  -----> finish build cluster "
minikube status

Text_Green " Your Done "
Text_Green "    Have fun with this cluster "

