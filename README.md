# local-environment-minikube

Easy way to install everything für our MSD and MEA Minikube Environment

<!-- TOC -->
* [local-environment-minikube](#local-environment-minikube)
* [First Steps](#first-steps)
* [Commands](#commands)
  * [Justfile specific](#justfile-specific)
  * [checks.just](#checksjust)
  * [infrastructure.just](#infrastructurejust)
  * [minikube.just](#minikubejust)
  * [services.just](#servicesjust)
* [Ports for services](#ports-for-services)
* [Rabbitmq](#rabbitmq)
* [Access to databases on Minikube to local machine](#access-to-databases-on-minikube-to-local-machine)
  * [MariaDB](#mariadb)
  * [PostgresQL](#postgresql)
* [planed features](#planed-features)
<!-- TOC -->


---

# First Steps

Please install `just` first<br>
https://github.com/casey/just<br>
https://cheatography.com/linux-china/cheat-sheets/justfile/

Testet on
- Windows 11 Powershell
- Windows 10 Powershell
- Arch Linux BASH
- macOS BASH UNTESTED


The file uses variables see `.env` file to change it.


# Commands

Every recipe is initialized with command `just` <br>
The Symbol `✔` in `accessible in main justfile` means, it's the same recipe call

## Justfile specific

All commands that are specific to the main-justfile

All recipes are accessible from: `just RECIPE`<br>
If `accessible in main justfile`: `just ALIAS`

| RECIPE                                 | accessible in main justfile | optional information                                                                                 |
|----------------------------------------|-----------------------------|------------------------------------------------------------------------------------------------------|
| changelog                              | changelog-all               | shows changelog of this justfile                                                                     |
| changelog-all                          | ✘                           | shows changelog of all justfiles                                                                     |
| check-all-mk-start                     | alias: `cs`                 | check-all and start minikube                                                                         |
| check-all-mk-start-infra               | alias: `csi`                | check-all and start minikube, then deploy infrastructure                                             |
| check-all-mk-start-infra-core-services | alias: `csic`, `all`        | check-all and start minikube, then deploy infrastructure, the install all core-services              |
| default                                | `--list`                    | shows all available recipes of this justfile                                                         |
| help                                   | help-all                    | shows help of this justfile                                                                          |
| help-all                               | ✘                           | shows help of all jusrfiles                                                                          |
| recipe-all                             | ✘                           | shows all recipes from all justfiles, even the recipes, that not imported to main-justfile           |
| rollout-status                         | alias: `rs`                 | used in difference recipes and files, rollout status of deployment,statefulset, daemonset in cluster |
| version                                | version-all                 | shows version of this justfile                                                                       |
| version-all                            | ✘                           | shows version of all justfiles                                                                       |


## checks.just

All recipes are accessible from: `just -f checks.just RECIPE`<br>
If `accessible in main justfile`: `just RECIPE`

| RECIPE               | accessible in main justfile | optional information                                                                                  |
|----------------------|-----------------------------|-------------------------------------------------------------------------------------------------------|
| changelog            | changelog-all               | shows changelog of this justfile                                                                      |
| check-all            | ✔ alias: `c`                | run all checks for programs, cpu, ram, docker                                                         |
| check-cpu            | ✔                           | writes output to missing,txt, or installed.txt                                                        |
| check-docker-running | ✔                           | writes output to missing,txt, or installed.txt                                                        |
| check-programs       | ✔                           | check a list if programs installed and minimal version is correct                                     |
| check-ram            | ✔                           | writes output to missing,txt, or installed.txt                                                        |
| clean                | ✔                           | clean up installed.txt and missing.txt                                                                |
| default              | checks                      | shows all available recipes of this justfile                                                          |
| help                 | help-all                    | shows help of this justfile                                                                           |
| show-installed       | ✔                           | show installed and running software (hardware) after running check-all, and single check call         |
| show-missing         | ✔                           | show not installed and not running software (hardware) after running check-all, and single check call |
| version              | version-all                 | shows version of this justfile                                                                        |


## infrastructure.just

All recipes are accessible from: `just -f infrastructure.just RECIPE`<br>
If `accessible in main justfile`: `just RECIPE`

| RECIPE                          | accessible in main justfile | optional information                                      |
|---------------------------------|-----------------------------|-----------------------------------------------------------|
| changelog                       | changelog-all               | shows changelog of this justfile                          |
| default                         | infrastructure              | shows all available recipes of this justfile              |
| deploy-cert-manager             | ✔                           | ✘                                                         |
| deploy-infra                    | ✔                           | deploy everything (that contains `deploy` in recipe-name |
| deploy-mariadb                  | ✔                           | ✘                                                         |
| deploy-postgres                 | ✔                           | ✘                                                         |
| deploy-prometheus               | ✔                           | ✘                                                         |
| deploy-rabbitmq                 | ✔                           | ✘                                                         |
| deploy-rabbitmq-kafka-connector | ✔                           | ✘                                                         |
| deploy-redpanda                 | ✔                           | ✘                                                         |
| deploy-redpanda-system          | ✔                           | ✘                                                         |
| deploy-redpanda-local-cluster   | ✔                           | ✘                                                         |
| help                            | help-all                    | shows help of this justfile                               |
| version                         | version-all                 | shows version of this justfile                            |


## minikube.just

All recipes are accessible from: `just -f minikube.just RECIPE`<br>
If `accessible in main justfile`: `just RECIPE`

| RECIPE    | accessible in main justfile | optional information                                                            |
|-----------|-----------------------------|---------------------------------------------------------------------------------|
| changelog | changelog-all               | shows changelog of this justfile                                                |
| default   | minikube                    | shows all available recipes of this justfile                                    |
| help      | help-all                    | shows help of this justfile                                                     |
| mk-delete | ✔                           | original command `minikube delete -all`                                         |
| mk-start  | ✔                           | orginal command `minikube start` with specific start-parameters                 |
| mk-status | ✔                           | checks, if minikube is running, but does not show the same as `minikube status` |
| mk-stop   | ✔                           | orginal command: `minikube stop`                                                |
| version   | version-all                 | shows version of this justfile                                                  |


## services.just

All recipes are accessible from: `just -f services.just RECIPE`<br>
If `accessible in main justfile`: `just RECIPE`

| RECIPE                   | accessible in main justfile | optional information                          |
|--------------------------|-----------------------------|-----------------------------------------------|
| changelog                | changelog-all               | shows changelog of this justfile              |
| default                  | services                    | shows all available recipes of this justfile  |
| help                     | help-all                    | shows help of this justfile                   |
| install *services        | ✔                           | enter list of services to deploy on cluster   |
| install-all              | ✔                           | install all core-services                     |
| remove *services         | ✔                           | enter list of services to remove from cluster |
| remove-all               | ✔                           | remove all core-services                      |
| update *services         | ✔                           | enter list of services to update on cluster   |
| update-all               | ✔                           | update all core-services                      |
| update-msd-repo          | ✔                           | update the helm-repo of msd-dungeon           |
| version                  | version-all                 | shows version of this justfile                |


---

# Ports for services

- The following ports will be exposed on UNIX and WINDOWS when using Docker Desktop after `minikube tunnel` on `127.0.0.1` 
- On UNIX with no Docker Desktop, the following ports will be exposed on `10.10.10.10` no minikube tunnel needed
- Accessible over NodePort

| service                     | nodePort | port  |
|-----------------------------|----------|-------|
| gamelog                     | 30001    | 8080  |
| game                        | 30002    | 8080  |
| map                         | 30003    | 8ß80  |
| robot                       | 30004    | 8080  |
| trading                     | 30005    | 8080  |
| rabbitmq-manager (external) | 31000    | 15672 |
| rabbitmq-amqp (internal)    | 31001    | 5672  |
| rabbitmq-epmd               | 31002    | 4369  |
| rabbitmq-dist               | 21003    | 25672 |

---
# Rabbitmq

- User: `admin`
- get password: `kubectl get secret --namespace rabbitmq rabbitmq -o jsonpath="{.data.rabbitmq-password}" | base64 -d`

# Access to databases on Minikube to local machine

## MariaDB
- Mariadb user `root` password: `kubectl get secret --namespace database mariadb-secret -o jsonpath="{.data.mariadb-root-password}" | base64 -d`
- Mariadb user `admin` password: `kubectl get secret --namespace database mariadb-secret -o jsonpath="{.data.mariadb-password}" | base64 -d`
- Mariadb user `game` password: `kubectl get secret --namespace database mariadb-user-secret -o jsonpath="{.data.game-password}" | base64 -d`
- Mariadb user `gamelog` password: `kubectl get secret --namespace database mariadb-user-secret -o jsonpath="{.data.gamelog-password}" | base64 -d`
- Mariadb user `trading` password: `kubectl get secret --namespace database mariadb-user-secret -o jsonpath="{.data.trading-password}" | base64 -d`
- Mariadb user `robot` password: `kubectl get secret --namespace database mariadb-user-secret -o jsonpath="{.data.robot-password}" | base64 -d` 
- Mariadb internal access in cluster `mariadb.database.svc.cluster.local:3306`
- Access Mariadb from external tools
  - `kubectl port-forward --namespace database svc/mariadb 3306:3306`
  - opens a local tunnel to `127.0.0.1:3306`
  - now you can access the mariadb under this address with Root, Admin, or Database-user Credentials


## PostgresQL
- Postgres root password: `kubectl get secret --namespace database postgresql-secret -o jsonpath="{.data.postgres-password}" | base64 -d`
- Postgres admin password: `kubectl get secret --namespace database postgresql-secret -o jsonpath="{.data.password}" | base64 -d`
- Postgres map password: `kubectl get secret --namespace database postgresql-user-secret -o jsonpath="{.data.map-password}" | base64 -d`
- Postgres internal access in cluster: `postgresql.database.svc.cluster.local:5432`
- Access Postgres from external tools
  - `kubectl port-forward --namespace database svc/postgresql 5432:5432`
  - opens a local tunnel to `127.0.0.1:5432`
  - now you can access the postgres under this address with Root, Admin, or Database-user Credentials

---
# planed features
- install an interaction with own player
- install and interaction with testplayers, to simulate other players


