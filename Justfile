# Quiet option to suppress intermediate outputs
set quiet := true

# Enable the dotenv feature to load variables from .env
set dotenv-load

# Set shell for Windows and Unix-based systems
set shell := ["bash", "-c"]
set windows-shell := ["powershell.exe", "-NoLogo", "-Command"]

# Aliases
alias c := check-all
alias cs := check-all-mk-start
alias csi := check-all-mk-start-infra
alias csic := check-all-mk-start-infra-core-services
alias all := check-all-mk-start-infra-core-services
alias rs := rollout-status

# Justfile specific information
just_script_version := "1.0.0"
script_changelog := """
- 0.0.0: Initial with check-runs, run-checks-start, run-checks-start-and-infra
- - 0.1.0: add difference for UNIX and WINDOWS
- - 0.2.0: add run-checks-start-and-infra-core-services
- - 0.3.0: add aliase
- - 0.4.0: migrate minikube.just recipes into Justfile
- - 0.5.0: add link to all recipes from all justfiles
- - - 0.5.1: add all recipes from checks.just
- - - 0.5.2: add all recipes from infrastructure.just
- - - 0.5.3: add all recipes from minikube.just
- - - 0.5.4: add all recipes from services.just
- - - 0.5.5: optimize Justfile own recipes and aliases
- - - 0.5.6: optimize output of recipe rollout-status
- - 0.6.0: add recipes for version, changelog, help
- - 0.7.0: add 'ALL' recipes
- - 0.8.0: Replace global variables with .env file
- - 0.9.0: Filling up recipe for help
- 1.0.0: Final release version 1.0.0
"""

help := """
Most commands from other Justfiles are available from main-Justfile
    just --list
  or
    just -l
  or
    just

To run commands:
    just RECIPE

The block INFO, INFO ALL, Justfile are main-Justfile specific commands
"""

############################################################
######## ---------------- Justfile ---------------- ########
############################################################

# Default recipe: List all recipes
[group('INFO')]
default:
    just -l

# Justfile Version
[group('INFO')]
version:
    echo "Version: {{just_script_version}}"

# Justfile Changelog
[group('INFO')]
changelog:
    echo "{{script_changelog}}"

# Help of recipe
[group('INFO')]
help:
    echo "{{help}}"

# Recipe for checking rollout status on Unix
[unix]
[group('Justfile')]
rollout-status:
    namespaces=$(kubectl get namespaces -o jsonpath='{.items[*].metadata.name}' | tr ' ' '\n'); \
    for ns in $namespaces; do \
        echo ""; \
        echo -e "\033[90m##############################################################\033[0m"; \
        echo -e "\033[90m# \033[34mChecking rollout status in namespace: $ns\033[90m #\033[0m"; \
        echo -e "\033[90m##############################################################\033[0m"; \
        resources=("statefulset" "deployment" "daemonset"); \
        for resource_type in ${resources[@]}; do \
            resource_names=$(kubectl get $resource_type -n $ns -o jsonpath='{.items[*].metadata.name}' | tr ' ' '\n'); \
            if [ -z "$resource_names" ]; then \
                echo -e "    \033[35mNo ${resource_type} found in namespace: $ns\033[0m"; \
            else \
                for resource_name in $resource_names; do \
                    echo -e "    \033[33mChecking rollout status of ${resource_type}/${resource_name}\033[0m"; \
                    rollout_output=$(kubectl rollout status ${resource_type}/${resource_name} -n ${ns}); \
                    echo "$rollout_output" | while IFS= read -r line; do \
                        if [[ "$line" == *"successfully rolled out"* ]]; then \
                            echo -e "    \033[32m$line\033[0m"; \
                        else \
                            echo "    $line"; \
                        fi; \
                    done; \
                done; \
            fi; \
            echo -e "    \033[90m------------------------------------------------------------\033[0m"; \
        done; \
        echo ""; \
        echo -e "\033[90m##############################################################\033[0m"; \
        echo -e "\033[90m# \033[90mEnd of rollout status check for namespace: $ns\033[90m #\033[0m"; \
        echo -e "\033[90m##############################################################\033[0m"; \
    done

# Recipe for checking rollout status on Windows
[windows]
[group('Justfile')]
rollout-status:
    $namespaces = (kubectl get namespaces -o jsonpath='{.items[*].metadata.name}').Split(" "); \
    foreach ($ns in $namespaces) { \
        Write-Host ""; \
        Write-Host "##############################################################" -ForegroundColor DarkGray; \
        Write-Host -NoNewline "# " -ForegroundColor DarkGray; \
        Write-Host "Checking rollout status in namespace: $ns" -ForegroundColor Blue -NoNewline; \
        Write-Host " #" -ForegroundColor DarkGray; \
        Write-Host "##############################################################" -ForegroundColor DarkGray; \
        $resources = @("statefulset", "deployment", "daemonset"); \
        foreach ($resource_type in $resources) { \
            $resource_names = kubectl get $resource_type -n $ns -o jsonpath='{.items[*].metadata.name}'; \
            if (-not [string]::IsNullOrEmpty($resource_names)) { \
                $resource_names = $resource_names.Split(" "); \
                foreach ($resource_name in $resource_names) { \
                    Write-Host ""; \
                    Write-Host "    Checking rollout status of $resource_type/$resource_name" -ForegroundColor Yellow; \
                    $rollout_output = kubectl rollout status $resource_type/$resource_name -n $ns; \
                    $rollout_output.Split("`n") | ForEach-Object { \
                        if ($_ -like "*successfully rolled out*") { \
                            Write-Host "    $_" -ForegroundColor Green; \
                        } else { \
                            Write-Host "    $_"; \
                        } \
                    }; \
                } \
            } else { \
                Write-Host ""; \
                Write-Host "    No $resource_type found in namespace: $ns" -ForegroundColor Magenta; \
            } \
            Write-Host "    ____________________________________________" -ForegroundColor DarkGray; \
        } \
        Write-Host ""; \
        Write-Host "##############################################################" -ForegroundColor DarkGray; \
        Write-Host -NoNewline "# " -ForegroundColor DarkGray; \
        Write-Host "End of rollout status check for namespace: $ns" -ForegroundColor DarkGray -NoNewline; \
        Write-Host " #" -ForegroundColor DarkGray; \
        Write-Host "##############################################################" -ForegroundColor DarkGray; \
    }



# Recipe to run checks, start Minikube if all checks pass, and deploy infrastructure for Unix
[unix]
[group('Justfile')]
check-all-mk-start:
    @just check-all
    @if [ ! -s "{{env_var('MISSING_FILE')}}" ]; then \
        just mk-start; \
    else \
        echo "Some checks failed. Please check {{env_var('MISSING_FILE')}} for details."; \
    fi

# Recipe to run checks, start Minikube if all checks pass, and deploy infrastructure for Windows
[windows]
[group('Justfile')]
check-all-mk-start:
    @just check-all
    @if (!(Test-Path '{{env_var('MISSING_FILE')}}') -or (Get-Content '{{env_var('MISSING_FILE')}}' | Measure-Object).Length -eq 0) { \
        just mk-start; \
    } else { \
        Write-Host "Some checks failed. Please check {{env_var('MISSING_FILE')}} for details."; \
    }

# Recipe to run checks, start Minikube, and deploy infrastructure for Unix
[unix]
[group('Justfile')]
check-all-mk-start-infra:
    @just check-all-mk-start
    @if minikube status | grep -q "Running"; then \
        echo "Minikube is running. Deploying infrastructure..."; \
        just deploy-infra; \
    else \
        echo "Minikube failed to start or is not running. Please check Minikube logs for details."; \
    fi

# Recipe to run checks, start Minikube, and deploy infrastructure for Windows
[windows]
[group('Justfile')]
check-all-mk-start-infra:
    @just check-all-mk-start
    @if ((minikube status | Select-String -Quiet "Running") -eq $true) { \
        Write-Host "Minikube is running. Deploying infrastructure..."; \
        just deploy-infra; \
    } else { \
        Write-Host "Minikube failed to start or is not running. Please check Minikube logs for details."; \
    }

# Recipe to run checks, start Minikube, deploy infrastructure, and deploy all core services on Unix
[unix]
[group('Justfile')]
check-all-mk-start-infra-core-services:
    @just check-all-mk-start-infra
    @if [ ! -s "{{env_var('MISSING_FILE')}}" ]; then \
        just install-all; \
    else \
        echo "Some checks failed in previous steps. Please check {{env_var('MISSING_FILE')}} for details."; \
    fi

# Recipe to run checks, start Minikube, deploy infrastructure, and deploy all core services on Windows
[windows]
[group('Justfile')]
check-all-mk-start-infra-core-services:
    @just check-all-mk-start-infra
    @if (!(Test-Path '{{env_var('MISSING_FILE')}}') -or (Get-Content '{{env_var('MISSING_FILE')}}' | Measure-Object).Length -eq 0) { \
        just install-all; \
    } else { \
        Write-Host "Some checks failed in previous steps. Please check {{env_var('MISSING_FILE')}} for details."; \
    }

#####################################################################
######## ---------------- INFO ALL Justfiles -------------- ########
#####################################################################

# Recipes from all Justfiles
[group('INFO ALL')]
recipe-all:
    @echo "Recipes in main Justfile:"
    @just --list
    @echo ""
    @echo "Recipes in checks.just:"
    @just -f "{{env_var('JUSTFILE_CHECKS')}}" --list
    @echo ""
    @echo "Recipes in infrastructure.just:"
    @just -f "{{env_var('JUSTFILE_INFRASTRUCTURE')}}" --list
    @echo ""
    @echo "Recipes in services.just:"
    @just -f "{{env_var('JUSTFILE_SERVICES')}}" --list
    @echo ""
    @echo "Recipes in minikube.just:"
    @just -f "{{env_var('JUSTFILE_MINIKUBE')}}" --list

# Changelog from all Justfiles
[group('INFO ALL')]
changelog-all:
    @echo "Changelog in main Justfile:"
    @just changelog
    @echo ""
    @echo "Changelog in checks.just:"
    @just -f "{{env_var('JUSTFILE_CHECKS')}}" changelog
    @echo ""
    @echo "Changelog in infrastructure.just:"
    @just -f "{{env_var('JUSTFILE_INFRASTRUCTURE')}}" changelog
    @echo ""
    @echo "Changelog in services.just:"
    @just -f "{{env_var('JUSTFILE_SERVICES')}}" changelog
    @echo ""
    @echo "Changelog in minikube.just:"
    @just -f "{{env_var('JUSTFILE_MINIKUBE')}}" changelog

# Help from all Justfiles
[group('INFO ALL')]
help-all:
    @echo "Help in main Justfile:"
    @just help
    @echo ""
    @echo "Help in checks.just:"
    @just -f "{{env_var('JUSTFILE_CHECKS')}}" help
    @echo ""
    @echo "Help in infrastructure.just:"
    @just -f "{{env_var('JUSTFILE_INFRASTRUCTURE')}}" help
    @echo ""
    @echo "Help in services.just:"
    @just -f "{{env_var('JUSTFILE_SERVICES')}}" help
    @echo ""
    @echo "Help in minikube.just:"
    @just -f "{{env_var('JUSTFILE_MINIKUBE')}}" help

# Version from all Justfiles on Unix
[unix]
[group('INFO ALL')]
version-all:
    @echo "Main Justfile: --------- $(just version | tail -n 1)"; \
    echo "checks.just: ----------- $(just -f '{{env_var('JUSTFILE_CHECKS')}}' version | tail -n 1)"; \
    echo "infrastructure.just: --- $(just -f '{{env_var('JUSTFILE_INFRASTRUCTURE')}}' version | tail -n 1)"; \
    echo "services.just: --------- $(just -f '{{env_var('JUSTFILE_SERVICES')}}' version | tail -n 1)"; \
    echo "minikube.just: --------- $(just -f '{{env_var('JUSTFILE_MINIKUBE')}}' version | tail -n 1)"

# Version from all Justfiles on Windows
[windows]
[group('INFO ALL')]
version-all:
    @Write-Host "Main Justfile: $(just version | Select-Object -Last 1)"; \
    Write-Host "checks.just: $(just -f '{{env_var('JUSTFILE_CHECKS')}}' version | Select-Object -Last 1)"; \
    Write-Host "infrastructure.just: $(just -f '{{env_var('JUSTFILE_INFRASTRUCTURE')}}' version | Select-Object -Last 1)"; \
    Write-Host "services.just: $(just -f '{{env_var('JUSTFILE_SERVICES')}}' version | Select-Object -Last 1)"; \
    Write-Host "minikube.just: $(just -f '{{env_var('JUSTFILE_MINIKUBE')}}' version | Select-Object -Last 1)"

###############################################################
######## ---------------- checks.just ---------------- ########
###############################################################

# Show default recipe of checks.just file
[group('checks.just')]
checks:
    just -f "{{env_var('JUSTFILE_CHECKS')}}" default

# Run all necessary checks from the checks.just file
[group('checks.just')]
check-all:
    just -f "{{env_var('JUSTFILE_CHECKS')}}" check-all

# Clean missing.txt and installed.txt from the checks.just file
[group('checks.just')]
clean:
    just -f "{{env_var('JUSTFILE_CHECKS')}}" clean

# Run check-cpu from the checks.just file
[group('checks.just')]
check-cpu:
    just -f "{{env_var('JUSTFILE_CHECKS')}}" check-cpu

# Run check-ram from the checks.just file
[group('checks.just')]
check-ram:
    just -f "{{env_var('JUSTFILE_CHECKS')}}" check-ram

# Run check-docker-running from the checks.just file
[group('checks.just')]
check-docker-running:
    just -f "{{env_var('JUSTFILE_CHECKS')}}" check-docker-running

# Run check-programs from the checks.just file
[group('checks.just')]
check-programs:
    just -f "{{env_var('JUSTFILE_CHECKS')}}" check-programs

# Run show-installed from the checks.just file
[group('checks.just')]
show-installed:
    just -f "{{env_var('JUSTFILE_CHECKS')}}" show-installed

# Run show-missing from the checks.just file
[group('checks.just')]
show-missing:
    just -f "{{env_var('JUSTFILE_CHECKS')}}" show-missing

#######################################################################
######## ---------------- infrastructure.just ---------------- ########
#######################################################################

# Show default recipe of infrastructure.just file
[group('infrastructure.just')]
infrastructure:
    just -f "{{env_var('JUSTFILE_INFRASTRUCTURE')}}" default

# Deploy all necessary middlewares from the infrastructure.just file
[group('infrastructure.just')]
deploy-infra:
    just -f "{{env_var('JUSTFILE_INFRASTRUCTURE')}}" deploy-infra

# Deploy cert-manager from the infrastructure.just file
[group('infrastructure.just')]
deploy-cert-manager:
    just -f "{{env_var('JUSTFILE_INFRASTRUCTURE')}}" deploy-cert-manager

# Deploy Prometheus from the infrastructure.just file
[group('infrastructure.just')]
deploy-prometheus:
    just -f "{{env_var('JUSTFILE_INFRASTRUCTURE')}}" deploy-prometheus

# Deploy redpanda-system from the infrastructure.just file
[group('infrastructure.just')]
deploy-redpanda-system:
    just -f "{{env_var('JUSTFILE_INFRASTRUCTURE')}}" deploy-redpanda-system

# Deploy redpanda local cluster from the infrastructure.just file
[group('infrastructure.just')]
deploy-redpanda-local-cluster:
    just -f "{{env_var('JUSTFILE_INFRASTRUCTURE')}}" deploy-redpanda-local-cluster

# Deploy redpanda (redpanda-system and redpanda-local-cluster) from the infrastructure.just file
[group('infrastructure.just')]
deploy-redpanda:
    just -f "{{env_var('JUSTFILE_INFRASTRUCTURE')}}" deploy-redpanda

# Deploy RabbitMQ from the infrastructure.just file
[group('infrastructure.just')]
deploy-rabbitmq:
    just -f "{{env_var('JUSTFILE_INFRASTRUCTURE')}}" deploy-rabbitmq

# Deploy rabbitmq-kafka-connector from the infrastructure.just file
[group('infrastructure.just')]
deploy-rabbitmq-kafka-connector:
    just -f "{{env_var('JUSTFILE_INFRASTRUCTURE')}}" deploy-rabbitmq-kafka-connector

# Deploy MariaDB from the infrastructure.just file
[group('infrastructure.just')]
deploy-mariadb:
    just -f "{{env_var('JUSTFILE_INFRASTRUCTURE')}}" deploy-mariadb

# Deploy PostgreSQL from the infrastructure.just file
[group('infrastructure.just')]
deploy-postgres:
    just -f "{{env_var('JUSTFILE_INFRASTRUCTURE')}}" deploy-postgres

#################################################################
######## ---------------- minikube.just ---------------- ########
#################################################################

# Show default recipe of minikube.just file
[group('minikube.just')]
minikube:
    just -f "{{env_var('JUSTFILE_MINIKUBE')}}" default

# Start Minikube cluster from the minikube.just file
[group('minikube.just')]
mk-start:
    just -f "{{env_var('JUSTFILE_MINIKUBE')}}" mk-start

# Check Minikube cluster status from the minikube.just file
[group('minikube.just')]
mk-status:
    just -f "{{env_var('JUSTFILE_MINIKUBE')}}" mk-status

# Stop Minikube cluster from the minikube.just file
[group('minikube.just')]
mk-stop:
    just -f "{{env_var('JUSTFILE_MINIKUBE')}}" mk-stop

# Delete Minikube cluster from the minikube.just file
[group('minikube.just')]
mk-delete:
    just -f "{{env_var('JUSTFILE_MINIKUBE')}}" mk-delete

#################################################################
######## ---------------- services.just ---------------- ########
#################################################################

# Show default recipe of services.just file
[group('services.just')]
services:
    just -f "{{env_var('JUSTFILE_SERVICES')}}" default

# Install all core services from the services.just file
[group('services.just')]
install-all:
    just -f "{{env_var('JUSTFILE_SERVICES')}}" install-all

# Update all core services from the services.just file
[group('services.just')]
update-all:
    just -f "{{env_var('JUSTFILE_SERVICES')}}" update-all

# Remove all core services from the services.just file
[group('services.just')]
remove-all:
    just -f "{{env_var('JUSTFILE_SERVICES')}}" remove-all

# Install chosen core services from the services.just file
[group('services.just')]
install *services:
    just -f "{{env_var('JUSTFILE_SERVICES')}}" install {{services}}

# Update chosen core services from the services.just file
[group('services.just')]
update *services:
    just -f "{{env_var('JUSTFILE_SERVICES')}}" update {{services}}

# Remove chosen core services from the services.just file
[group('services.just')]
remove *services:
    just -f "{{env_var('JUSTFILE_SERVICES')}}" remove {{services}}

# Check chosen core services from the services.just file
[group('services.just')]
check-services *services:
    just -f "{{env_var('JUSTFILE_SERVICES')}}" check-services {{services}}

# Update the MSD Helm Repo from the services.just file
[group('services.just')]
update-msd-repo:
    just -f "{{env_var('JUSTFILE_SERVICES')}}" update-msd-repo
